% Suivi SIO
% par [moulinux](https://moulinux.frama.io/)
% (révision 01.02.2021)

## Préparation de l'épreuve E4

Télécharger les [notes] de la présentation

[notes]: notes.pdf

## Introduction

* Bonjour, je m'appelle...
* Je suis actuellement en BTS SIO
* J'ai choisi la spécialité SLAM

## Contexte AMAP

* Nous travaillons pour le compte de la société fictive **2lo**
* Le client : Le réseau AMAP Auvergne

## Contexte AMAP
### 2lo

* Logiciel libre pour les organisations
* Méthodes agiles
* Scop

## Contexte AMAP
### ![logo](images/logo-amap-auvergne.png "Logo réseau...")

* Une AMAP repose sur des partenariats entre des paysans responsables et des *consom'acteurs*
* Le réseau des AMAP Auvergne est la structure régionale de ce mouvement


## Situation Professionnelle n°1

* Créer le site de présentation du réseau AMAP-Auvergne
* Le thème est basé sur celui d'un réseau existant
* J'ai travaillé sur le thème [HN](http://reseau-amap-hn.com/) avec **Django CMS**

## SP n°1
![Capture de la page d'accueil...](images/SP1/accueil.png "Page d'accueil")

## SP n°1
![Capture de l'interface d'administration...](images/SP1/admin_pages.png "Interface d'administration")

## SP n°1
![Capture d'une modification de page...](images/SP1/mod_page.png "Modification d'une page")

## SP n°1
### B1.4.1 Analyser les objectifs et les modalités d’organisation d’un projet
#### auto-évaluation : en cours d'acquisition
![Capture du cahier des charges](images/SP1/contenu.png)

## SP n°1
### B1.4.3 Évaluer les indicateurs de suivi d’un projet et analyser les écarts
#### auto-évaluation : balbutiante
![Capture du diagramme de Gantt](images/SP1/projet.png)

## SP n°1
### B1.3.3 Participer à l’évolution d’un site Web exploitant les données de l’organisation
#### auto-évaluation : acquis
![Capture du template base.html](images/SP1/template_base.png)

## SP n°1
### B1.5.3 Accompagner les utilisateurs dans la mise en place d’un service
#### auto-évaluation : en cours d'acquisition
![Capture de la documentation utilisateur](images/SP1/documentation.png "Manuel d'utilisation")

## SP n°1
### B1.6.2 Mettre en œuvre des outils et stratégies de veille informationnelle (veille active)
#### auto-évaluation : en cours d'acquisition
* Recherche pour une navigation interne à une rubrique
* Mots clés *django cms show menu only sub pages*
* [Identification] de la fonction fonction `{% show_sub_menu 1 1 100 "submenu.html" %}`

[Identification]: http://docs.django-cms.org/en/latest/reference/navigation.html

## SP n°1
### B1.6.2 Mettre en œuvre des outils et stratégies de veille informationnelle (veille passive)
#### auto-évaluation : en cours d'acquisition
* chat : #django-cms on freenode.net
* Mailing Lists (google groups) : [user][userdjangocms] and [dev][devdjangocms]
* Issue [Tracker][trackerdjangocms]

[userdjangocms]: https://groups.google.com/forum/#!forum/django-cms
[devdjangocms]: https://groups.google.com/forum/#!forum/django-cms-developers
[trackerdjangocms]: https://github.com/divio/django-cms/issues

## Situation Professionnelle n°2

* Correction de bogues sur l'application AMAphp
* ...

## SP n°2
### B1.2.3 Traiter des demandes concernant les applications
#### auto-évaluation : En cours d'acquisition

```php
    /**
     * Contrôleur pour la liste des paysans.
     *
     * @Route("/paysan", name="paysans")
     */
    public function listePaysans()
    {
        $pdo = PdoAmap::getPdoAmap();
        $jeuPaysan = $pdo->obtenirPaysans();
        return $this->render('listePaysan.html.twig', array('lesPaysans' => $jeuPaysan));
    }
```
