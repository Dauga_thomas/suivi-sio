# Reformulation(s) des activités du portefeuilles des compétences

> L'objectif est de préparer le texte, les notes, accompagnant votre
présentation pour l'épreuve E6 du BTS SIO. Reformuler permet
également de vérifier que vous n'êtes par hors-sujet sur vos explications,
voir sur la compétence visée

## Situation Professionnelle n°1

### B1.4.1 Analyser les objectifs et les modalités d’organisation d’un projet
* auto-évaluation : en cours d'acquisition

> J'ai bien lu le cahier des charges que l'on m'a fourni. Mais je ne l'ai
pas analysé suffisamment pour en faire un document de synthèse qui
m'aurait permis de planifier mon travail par exemple.

### B1.4.3 Évaluer les indicateurs de suivi d’un projet et analyser les écarts
* auto-évaluation : balbutiante

> Bien que mon chef de projet ait préparé des tâches en attente sur le
service [framaboard], je n'ai pas pris le temps de paramétrer les tâches
sur lesquels j'étais amené à travaillé (date de début, date de fin, attribution
du rôle), je n'ai pas décomposé cette tâche, en sous-tâches de façon à ce que
la tâche devienne quantifiable dans la gestion du projet.

[framaboard]: https://albertlondres.framaboard.org/

### B1.3.3 Participer à l’évolution d’un site Web exploitant les données de l’organisation
* auto-évaluation : acquis

> Dans le cadre de la réalisation du site, j'ai travaillé sur la modification
de fichiers *templates*, qui intégraient des éléments de styles que nous dévions
télécharger sur un site existant.

> J'ai réussi à modifier les fichiers templates que l'on m'avait fourni
en guise de démonstration. Même si je ne suis pas devenu un expert de la
syntaxe des gabarits **Django**, je pense être capable de refaire cela
par la suite.

### B1.5.3 Accompagner les utilisateurs dans la mise en place d’un service
* auto-évaluation : en cours d'acquisition

> J'ai participé à la rédaction d'une documentation permettant à d'autres
développeurs de comprendre ma démarche pour la réalisation d'un site avec
**Django CMS**.

> Ma documentation était basée sur le tutoriel fourni par mon chef de projet.
Je n'ai pas intégré les contraintes du cahier des charges sur le style et
la table des matières. La rédaction du document n'était de plus pas suffisamment
soignée.

### B1.6.2 Mettre en œuvre des outils et stratégies de veille informationnelle (veille active)
* auto-évaluation : en cours d'acquisition
* Recherche pour une navigation interne à une rubrique
* Mots clés *django cms show menu only sub pages*
* [Identification] de la fonction fonction `{% show_sub_menu 1 1 100 "submenu.html" %}`

[Identification]: http://docs.django-cms.org/en/latest/reference/navigation.html

### B1.6.2 Mettre en œuvre des outils et stratégies de veille informationnelle (veille passive)
* auto-évaluation : en cours d'acquisition
* chat : #django-cms on freenode.net
* Mailing Lists (google groups) : [user][userdjangocms] and [dev][devdjangocms]
* Issue [Tracker][trackerdjangocms]

[userdjangocms]: https://groups.google.com/forum/#!forum/django-cms
[devdjangocms]: https://groups.google.com/forum/#!forum/django-cms-developers
[trackerdjangocms]: https://github.com/divio/django-cms/issues

## Situation Professionnelle n°2

### B1.2.3 Traiter des demandes concernant les applications
* auto-évaluation : en cours d'acquisition
