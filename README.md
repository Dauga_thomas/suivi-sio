# Gestion d'un portefeuille de compétences professionnelles

Dans le cadre de la préparation de l'épreuve E4 – Support et mise à disposition de services informatiques,
les étudiants doivent réaliser un dossier numérique contenant l'ensemble
des éléments justifiant leur parcours de professionnalisation.
Pour chaque réalisation présente dans le dossier, les [compétences][competences]
du bloc B1 mobilisées doivent être précisées.

Ce projet démontre également comment faire une [présentation] à partir d'un
portefeuille de compétences. La présentation est générée via l'intégration
continue gitlab-ci avec le logiciel [pandoc] et le *framework* [reveal.js].

Bien évidemment, le projet est adaptable pour n'importe quel contenu de présentation
(et pas uniquement la constitution d'un portefeuille de compétences).

## Exploitation du projet

Il suffit de modifier les fichiers `presentation.md` et `notes.md`.
A chaque `commit` sur la branche `master`, le moteur d'intégration continue va générer une
[présentation], au format HTML (javascript), ainsi qu'un document au format PDF pour les notes.
Les pages de gitlab-ce permettent ensuite de [visualiser][présentation] cette présentation.

**IMPORTANT**: Afin de ne pas surcharger les serveurs de framasoft,
les utilisateurs sont invités à tester en local leurs modifications de contenu.
Il suffit pour cela de reprendre les commandes exploitées dans le fichier `.gitlab-ci.yml` :

* Pour générer les notes au format PDF : `pandoc -o notes.pdf --variable documentclass=article --variable fontsize=20pt --number-sections --pdf-engine=xelatex --highlight-style tango notes.md`
* Pour générer la présentation au format HTML (javascript) : `pandoc -o index.html -s -t revealjs -i --slide-level=2 -V theme="solarized" -V revealjs-url=https://unpkg.com/reveal.js@3.9.2/ ./presentation.md`

> Les commandes ci-dessus supposent que les paquets `pandoc` et `texlive-xetex`
sont installés sur le système. Pour une distribution Debian, il suffit, une
fois encore de reprendre la commande du fichier `.gitlab-ci.yml` :
`apt-get update && apt-get -y install pandoc && apt-get -y install texlive-xetex`

### Pandoc et la présentation

En utilisant l'option `--slide-level=2` de pandoc, les titres de niveau 2
permettent de distinguer les diapos (ou *slides* en anglais) :

* Un titre de niveau identique au `level_number` (dans notre cas de niveau **2**) débute une nouvelle diapo;
* Un titre de niveau inférieur au `level_number` (par exemple de niveau 3) génère un sous-titre dans la diapo courante;
* Un titre de niveau supérieur au `level_number` (par exemple de niveau 1) génère une diapo de titre ne contenant que celui-ci.

L'option `-i` de pandoc permet de faire apparaître les éléments des listes
progressivement (à chaque clic de souris, ou de pression sur la barre esoace, etc).

Le projet est configuré sur le thème `solarized`, il suffit bien évidemment
de modifier celui-ci par une autre thème prédéfini pour modifier l'habillage
de la présentation.

### Pandoc et le PDF

La génération des notes au format PDF exploite une fois encore le thème
installé par défaut sur la distribution. Il est, bien sûr, tout à fait
possible de changer l'habillage, par exemple en suivant les exemples
fournis sur ce [dépôt](https://framagit.org/inkhey/pandoc-template).

## Exploitation pour le Suivi-SIO

L'enseignant clone ce projet dans des dépôts privés, où les étudiants peuvent
contribuer. Ainsi, les étudiants peuvent alimenter progressivement leur
portefeuille de compétences au fur et à mesure de la progression (AP, stages).

L'enseignant a la possibilité de commenter ou partager des éléments de réflexion en
ouvrant des tickets sur les différentes situations professionnelles.

> **IMPORTANT**: En plus d'illustrer les compétences par des captures d'écran ou des extraits
de code pour leur présentation, les étudiants devront, **pour chaque compétence**, partager un
résumé qui leur permettra de préparer les commentaires qu'ils devront
apporter durant leur présentation.

[présentation]: https://slam.frama.io/suivi-sio
[pandoc]: https://pandoc.org
[reveal.js]: https://revealjs.com
[competences]: https://s2sio.frama.io/competences
